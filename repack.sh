echo "Cleanup old rom files"
rm -rf image

mkdir image
echo "Making erofs image"
sudo mkfs.erofs image/system.img system
sudo mkfs.erofs image/vendor.img vendor
sudo mkfs.erofs image/system_ext.img system_ext
sudo mkfs.erofs image/odm.img odm
sudo mkfs.erofs image/my_stock.img my_stock
sudo mkfs.erofs image/my_region.img my_region
sudo mkfs.erofs image/my_heytap.img my_heytap
sudo mkfs.erofs image/my_enginering.img my_enginering
sudo mkfs.erofs image/my_carrier.img my_carrier
echo "Copying raw image"
cp ../dump/product.img image/product.img
cp ../dump/my_product.img image/my_product.img
echo "Repack super image"
lpmake --device-size 11274289152 \
       --metadata-size 65536     \
       --metadata-slots 2        \
       --group qti_dynamic_partitions:11270094848 \
       --partition system:readonly:1105022976:qti_dynamic_partitions --image system=image/system.img \
       --partition vendor:readonly:785772544:qti_dynamic_partitions --image vendor=image/vendor.img \
       --partition system_ext:readonly:1437392896:qti_dynamic_partitions --image system_ext=image/system_ext.img \
       --partition odm:readonly:2304798720:qti_dynamic_partitions --image odm=image/odm.img \
       --partition my_stock:readonly:1587490816:qti_dynamic_partitions --image my_stock=image/my_stock.img \
       --partition my_region:readonly:6696960:qti_dynamic_partitions --image my_region=image/my_region.img \
       --partition my_heytap:readonly:841482240:qti_dynamic_partitions --image my_heytap=image/my_heytap.img \
       --partition my_enginering:readonly:335872:qti_dynamic_partitions --image my_enginering=image/my_enginering.img \
       --partition my_carrier:readonly:335872:qti_dynamic_partitions --image my_carrier=image/my_carrier.img \
       --partition product:readonly:12754944:qti_dynamic_partitions --image product=image/product.img \
       --partition my_product:readonly:408289280:qti_dynamic_partitions --image my_product=image/my_product.img \
       -o image/super.img        